// Import the functions you need from the SDKs you need
import { getApp, getApps, initializeApp } from "firebase/app";
import { GoogleAuthProvider, getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCZ0GIqhG5H7I6mkJz3MLfZjIfP7u7hCXQ",
  authDomain: "my-app-52afb.firebaseapp.com",
  projectId: "my-app-52afb",
  storageBucket: "my-app-52afb.appspot.com",
  messagingSenderId: "753210824129",
  appId: "1:753210824129:web:0737c0fac1b2f17c018b40"
};

// Initialize Firebase
const app = getApps().length ? getApp() : initializeApp(firebaseConfig)

const db = getFirestore(app)

const auth = getAuth(app)

const provider = new GoogleAuthProvider()

export { db, auth, provider }
